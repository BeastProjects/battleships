# Additional clean files
cmake_minimum_required(VERSION 3.16)

if("${CONFIG}" STREQUAL "" OR "${CONFIG}" STREQUAL "Debug")
  file(REMOVE_RECURSE
  "BattleshipsGUI_autogen"
  "CMakeFiles\\BattleshipsGUI_autogen.dir\\AutogenUsed.txt"
  "CMakeFiles\\BattleshipsGUI_autogen.dir\\ParseCache.txt"
  )
endif()
