/********************************************************************************
** Form generated from reading UI file 'signupscreen.ui'
**
** Created by: Qt User Interface Compiler version 6.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SIGNUPSCREEN_H
#define UI_SIGNUPSCREEN_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>

QT_BEGIN_NAMESPACE

class Ui_SignupScreen
{
public:
    QPushButton *btnSignup;
    QTextEdit *passwordTextEdit;
    QLabel *label_2;
    QTextEdit *textEdit;
    QLabel *lblTitle;
    QLabel *label;
    QPushButton *btnReturn;
    QLabel *label_3;
    QTextEdit *mailTextEdit;
    QLabel *label_4;
    QTextEdit *momTextEdit;
    QTextEdit *ageTextEdit;
    QLabel *label_5;

    void setupUi(QDialog *SignupScreen)
    {
        if (SignupScreen->objectName().isEmpty())
            SignupScreen->setObjectName("SignupScreen");
        SignupScreen->resize(937, 661);
        btnSignup = new QPushButton(SignupScreen);
        btnSignup->setObjectName("btnSignup");
        btnSignup->setGeometry(QRect(360, 510, 241, 81));
        QIcon icon;
        icon.addFile(QString::fromUtf8("../helpme/sign-up.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnSignup->setIcon(icon);
        btnSignup->setIconSize(QSize(400, 100));
        passwordTextEdit = new QTextEdit(SignupScreen);
        passwordTextEdit->setObjectName("passwordTextEdit");
        passwordTextEdit->setGeometry(QRect(400, 230, 331, 51));
        QFont font;
        font.setPointSize(20);
        passwordTextEdit->setFont(font);
        label_2 = new QLabel(SignupScreen);
        label_2->setObjectName("label_2");
        label_2->setGeometry(QRect(170, 240, 200, 51));
        QFont font1;
        font1.setFamilies({QString::fromUtf8("Comic Sans MS")});
        font1.setPointSize(16);
        font1.setItalic(true);
        label_2->setFont(font1);
        textEdit = new QTextEdit(SignupScreen);
        textEdit->setObjectName("textEdit");
        textEdit->setGeometry(QRect(400, 170, 331, 51));
        textEdit->setFont(font);
        lblTitle = new QLabel(SignupScreen);
        lblTitle->setObjectName("lblTitle");
        lblTitle->setGeometry(QRect(270, 30, 521, 111));
        QFont font2;
        font2.setFamilies({QString::fromUtf8("Comic Sans MS")});
        font2.setPointSize(26);
        lblTitle->setFont(font2);
        label = new QLabel(SignupScreen);
        label->setObjectName("label");
        label->setGeometry(QRect(170, 170, 200, 51));
        label->setFont(font1);
        btnReturn = new QPushButton(SignupScreen);
        btnReturn->setObjectName("btnReturn");
        btnReturn->setGeometry(QRect(40, 580, 131, 51));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8("../helpme/button_return.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnReturn->setIcon(icon1);
        btnReturn->setIconSize(QSize(400, 100));
        label_3 = new QLabel(SignupScreen);
        label_3->setObjectName("label_3");
        label_3->setGeometry(QRect(160, 410, 241, 51));
        label_3->setFont(font1);
        mailTextEdit = new QTextEdit(SignupScreen);
        mailTextEdit->setObjectName("mailTextEdit");
        mailTextEdit->setGeometry(QRect(400, 290, 331, 51));
        mailTextEdit->setFont(font);
        label_4 = new QLabel(SignupScreen);
        label_4->setObjectName("label_4");
        label_4->setGeometry(QRect(170, 300, 200, 51));
        label_4->setFont(font1);
        momTextEdit = new QTextEdit(SignupScreen);
        momTextEdit->setObjectName("momTextEdit");
        momTextEdit->setGeometry(QRect(400, 410, 331, 51));
        momTextEdit->setFont(font);
        ageTextEdit = new QTextEdit(SignupScreen);
        ageTextEdit->setObjectName("ageTextEdit");
        ageTextEdit->setGeometry(QRect(400, 350, 331, 51));
        ageTextEdit->setFont(font);
        label_5 = new QLabel(SignupScreen);
        label_5->setObjectName("label_5");
        label_5->setGeometry(QRect(160, 350, 241, 51));
        label_5->setFont(font1);

        retranslateUi(SignupScreen);

        QMetaObject::connectSlotsByName(SignupScreen);
    } // setupUi

    void retranslateUi(QDialog *SignupScreen)
    {
        SignupScreen->setWindowTitle(QCoreApplication::translate("SignupScreen", "Dialog", nullptr));
        btnSignup->setText(QString());
        label_2->setText(QCoreApplication::translate("SignupScreen", "PASSWORD:", nullptr));
        lblTitle->setText(QCoreApplication::translate("SignupScreen", "BATTLESHIPS", nullptr));
        label->setText(QCoreApplication::translate("SignupScreen", "USERNAME:", nullptr));
        btnReturn->setText(QString());
        label_3->setText(QCoreApplication::translate("SignupScreen", "MOM'S NAME:", nullptr));
        label_4->setText(QCoreApplication::translate("SignupScreen", "MAIL:", nullptr));
        label_5->setText(QCoreApplication::translate("SignupScreen", "YOUR AGE:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SignupScreen: public Ui_SignupScreen {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SIGNUPSCREEN_H
