/********************************************************************************
** Form generated from reading UI file 'loginscreen.ui'
**
** Created by: Qt User Interface Compiler version 6.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINSCREEN_H
#define UI_LOGINSCREEN_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LoginScreen
{
public:
    QWidget *centralwidget;
    QPushButton *btnLogin;
    QLabel *lblTitle;
    QTextEdit *textEdit;
    QLabel *label;
    QPushButton *btnSignup;
    QTextEdit *passwordTextEdit;
    QLabel *label_2;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *LoginScreen)
    {
        if (LoginScreen->objectName().isEmpty())
            LoginScreen->setObjectName("LoginScreen");
        LoginScreen->resize(800, 600);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(LoginScreen->sizePolicy().hasHeightForWidth());
        LoginScreen->setSizePolicy(sizePolicy);
        QIcon icon;
        icon.addFile(QString::fromUtf8("../../Users/test0/Downloads/chess-board-of-rounded-square-shape.png"), QSize(), QIcon::Normal, QIcon::Off);
        LoginScreen->setWindowIcon(icon);
        LoginScreen->setStyleSheet(QString::fromUtf8("background-color: rgb(15, 91, 255);"));
        centralwidget = new QWidget(LoginScreen);
        centralwidget->setObjectName("centralwidget");
        btnLogin = new QPushButton(centralwidget);
        btnLogin->setObjectName("btnLogin");
        btnLogin->setGeometry(QRect(210, 350, 415, 91));
        btnLogin->setStyleSheet(QString::fromUtf8("background-color: rgb(138, 255, 65);\n"
"color: rgb(190, 232, 255);"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8("../helpme/button (1).png"), QSize(), QIcon::Normal, QIcon::Off);
        btnLogin->setIcon(icon1);
        btnLogin->setIconSize(QSize(1000, 1000));
        lblTitle = new QLabel(centralwidget);
        lblTitle->setObjectName("lblTitle");
        lblTitle->setGeometry(QRect(210, 40, 711, 111));
        QFont font;
        font.setFamilies({QString::fromUtf8("Comic Sans MS")});
        font.setPointSize(26);
        lblTitle->setFont(font);
        textEdit = new QTextEdit(centralwidget);
        textEdit->setObjectName("textEdit");
        textEdit->setGeometry(QRect(320, 180, 331, 51));
        QFont font1;
        font1.setPointSize(20);
        textEdit->setFont(font1);
        label = new QLabel(centralwidget);
        label->setObjectName("label");
        label->setGeometry(QRect(110, 180, 200, 51));
        QFont font2;
        font2.setFamilies({QString::fromUtf8("Comic Sans MS")});
        font2.setPointSize(16);
        font2.setItalic(true);
        label->setFont(font2);
        btnSignup = new QPushButton(centralwidget);
        btnSignup->setObjectName("btnSignup");
        btnSignup->setGeometry(QRect(300, 470, 241, 81));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8("../helpme/sign-up.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnSignup->setIcon(icon2);
        btnSignup->setIconSize(QSize(400, 100));
        passwordTextEdit = new QTextEdit(centralwidget);
        passwordTextEdit->setObjectName("passwordTextEdit");
        passwordTextEdit->setGeometry(QRect(320, 250, 331, 51));
        passwordTextEdit->setFont(font1);
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName("label_2");
        label_2->setGeometry(QRect(110, 250, 200, 51));
        label_2->setFont(font2);
        LoginScreen->setCentralWidget(centralwidget);
        statusbar = new QStatusBar(LoginScreen);
        statusbar->setObjectName("statusbar");
        LoginScreen->setStatusBar(statusbar);

        retranslateUi(LoginScreen);

        QMetaObject::connectSlotsByName(LoginScreen);
    } // setupUi

    void retranslateUi(QMainWindow *LoginScreen)
    {
        LoginScreen->setWindowTitle(QCoreApplication::translate("LoginScreen", "LoginScreen", nullptr));
        btnLogin->setText(QString());
        lblTitle->setText(QCoreApplication::translate("LoginScreen", "BATTLESHIPS", nullptr));
        label->setText(QCoreApplication::translate("LoginScreen", "USERNAME:", nullptr));
        btnSignup->setText(QString());
        label_2->setText(QCoreApplication::translate("LoginScreen", "PASSWORD:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class LoginScreen: public Ui_LoginScreen {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINSCREEN_H
