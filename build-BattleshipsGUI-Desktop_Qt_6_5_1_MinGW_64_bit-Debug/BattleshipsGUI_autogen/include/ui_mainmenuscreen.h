/********************************************************************************
** Form generated from reading UI file 'mainmenuscreen.ui'
**
** Created by: Qt User Interface Compiler version 6.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINMENUSCREEN_H
#define UI_MAINMENUSCREEN_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_MainMenuScreen
{
public:
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;

    void setupUi(QDialog *MainMenuScreen)
    {
        if (MainMenuScreen->objectName().isEmpty())
            MainMenuScreen->setObjectName("MainMenuScreen");
        MainMenuScreen->resize(688, 573);
        QIcon icon;
        icon.addFile(QString::fromUtf8("../photos/chess-board-of-rounded-square-shape.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainMenuScreen->setWindowIcon(icon);
        MainMenuScreen->setStyleSheet(QString::fromUtf8("background-color: rgb(95, 32, 255);"));
        pushButton = new QPushButton(MainMenuScreen);
        pushButton->setObjectName("pushButton");
        pushButton->setGeometry(QRect(130, 40, 421, 91));
        pushButton->setMaximumSize(QSize(451, 91));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8("../helpme/button.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton->setIcon(icon1);
        pushButton->setIconSize(QSize(1000, 1000));
        pushButton_2 = new QPushButton(MainMenuScreen);
        pushButton_2->setObjectName("pushButton_2");
        pushButton_2->setGeometry(QRect(130, 198, 421, 91));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8("../helpme/button_join-game (2).png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_2->setIcon(icon2);
        pushButton_2->setIconSize(QSize(500, 500));
        pushButton_3 = new QPushButton(MainMenuScreen);
        pushButton_3->setObjectName("pushButton_3");
        pushButton_3->setGeometry(QRect(130, 420, 421, 111));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8("../helpme/button_sign-out (2).png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_3->setIcon(icon3);
        pushButton_3->setIconSize(QSize(500, 500));

        retranslateUi(MainMenuScreen);

        QMetaObject::connectSlotsByName(MainMenuScreen);
    } // setupUi

    void retranslateUi(QDialog *MainMenuScreen)
    {
        MainMenuScreen->setWindowTitle(QCoreApplication::translate("MainMenuScreen", "Dialog", nullptr));
        pushButton->setText(QString());
        pushButton_2->setText(QString());
        pushButton_3->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainMenuScreen: public Ui_MainMenuScreen {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINMENUSCREEN_H
