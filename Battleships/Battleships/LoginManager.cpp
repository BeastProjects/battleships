#include "LoginManager.h"

// Constructor
LoginManager::LoginManager() : m_users() {}

//This adds a user to the user's vector 
const bool LoginManager::addNewUser(std::string username, std::string password) 
{
	if(!isUserLogged(username) && username.length() <= CHARACTERS_LIMIT)
	{
		this->m_users.insert({ username, password });

		return true;
	}

	return false;
}

//Returns if a user of the same name is already logged in
const bool LoginManager::isUserLogged(std::string username) const
{
	for (const auto& userEntry : this->m_loggedUsers)
	{
		if (userEntry == username)
		{
			return true;
		}
	}

	return false;
}

const bool LoginManager::doesPasswordMatch(std::string username, std::string password) const
{
	for (const auto& userEntry : this->m_users)
	{
		if (userEntry.first == username && userEntry.second == password)
		{
			return true;
		}
	}

	return false;
}

// Returns true if the a username is already taken
const bool LoginManager::doesUserExist(std::string username) const
{
	for (const auto& userEntry : this->m_users)
	{
		if (userEntry.first == username)
		{
			return true;
		}
	}

	return false;
}

void LoginManager::removeUser(std::string username)
{
	for (auto i = m_loggedUsers.begin(); i != m_loggedUsers.end(); i++)
	{
		if (*i == username)
		{
			m_loggedUsers.erase(i);
			break;
		}
	}
}

void LoginManager::logUser(std::string username)
{
	if (!isUserLogged(username))
	{
		this->m_loggedUsers.push_back(username);
	}
}


