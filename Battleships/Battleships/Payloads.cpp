#include "Payloads.h"

LoginPayload::LoginPayload(const std::string data) : IPayload(data) {}

unsigned int LoginPayload::getCode() const
{
	return LOGIN;
}

SignupPayload::SignupPayload(const std::string data) : IPayload(data) {}

unsigned int SignupPayload::getCode() const
{
	return SIGNUP;
}

SignoutPayload::SignoutPayload(const std::string data) : IPayload(data) {}

unsigned int SignoutPayload::getCode() const
{
	return SIGNOUT;
}

LeavePayload::LeavePayload(const std::string data) : IPayload(data) {}

unsigned int LeavePayload::getCode() const
{
	return RESIGN;
}

CreateGamePayload::CreateGamePayload(const std::string data) : IPayload(data) {}

unsigned int CreateGamePayload::getCode() const
{
	return CREATE_GAME;
}

GameMovePayload::GameMovePayload(const std::string data) : IPayload(data) {}

unsigned int GameMovePayload::getCode() const
{
	return GAME_MOVE;
}

SetBoardPayload::SetBoardPayload(const std::string data) : IPayload(data) {}

unsigned int SetBoardPayload::getCode() const
{
	return SET_BOARD;
}

JoinGamePayload::JoinGamePayload(const std::string data) : IPayload(data) {}

unsigned int JoinGamePayload::getCode() const
{
	return JOIN_GAME;
}

UpdateGamePayload::UpdateGamePayload(const std::string data) : IPayload(data) {}

unsigned int UpdateGamePayload::getCode() const
{
	return UPDATE_GAME;
}

VerificationPayload::VerificationPayload(const std::string data) : IPayload(data) {}

unsigned int VerificationPayload::getCode() const
{
	return VERIFICATION;
}

ErrorPayload::ErrorPayload(const std::string data) : IPayload(data) {}

unsigned int ErrorPayload::getCode() const
{
	return ERROR;
}

VictoryPayload::VictoryPayload(const std::string data) : IPayload(data) {}

unsigned int VictoryPayload::getCode() const
{
	return VICTORY;
}

LossPayload::LossPayload(const std::string data) : IPayload(data) {}

unsigned int LossPayload::getCode() const
{
	return LOSS;
}

StartGamePayload::StartGamePayload(const std::string data) : IPayload(data) {}

unsigned int StartGamePayload::getCode() const
{
	return START_GAME;
}