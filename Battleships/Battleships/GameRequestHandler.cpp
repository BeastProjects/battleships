#include "GameRequestHandler.h"

char* err_msg = 0;

GameRequestHandler::GameRequestHandler(UserInfo userData) : m_isFirstPlayer(userData.isFirstPlayer), m_game(userData.game),
m_user(userData.username), m_gameStarted(userData.gameStarted) 
{}

const StateControl GameRequestHandler::handleRequest(IPayload_ptr request)
{
	StateControl result;

	unsigned int code = request->getCode();

	try
	{
		switch (code)
		{
		case GAME_MOVE:
			result = this->gameMove(request);
			break;

		case UPDATE_GAME:
			result = this->updateGame();
			break;

		case RESIGN:
			result = this->resign();
			break;

		default:
			result = generateErrorResponse("Irrelevant Game Request");
		}
	}
	catch (const std::exception& exp)
	{
		std::shared_ptr<IPayload> payload = std::make_shared<ErrorPayload>(exp.what());
		result = { menuState, payload, "", nullptr };
	}

	return result;
}

//for example 0011 is a move payload
const StateControl GameRequestHandler::gameMove(IPayload_ptr request)
{
	StateControl state;
	std::string data = request->getData();

	state.userInfo.username = m_user;
	state.userInfo.game = m_game;
	state.userInfo.isFirstPlayer = m_isFirstPlayer;
	state.newState = gameState;

	if(m_game->getTurn() != this->m_isFirstPlayer)
	{
		state.response = std::make_shared<ErrorPayload>("Not Your Turn");
		return state;
	}

	coordinate x = data[0];
	coordinate y = data[1];
	
	char moveResult = m_game->makeMove({x, y});

	if (moveResult == 0)
	{
		return generateErrorResponse("Illegal Move");
	}

	state.response = std::make_shared<VerificationPayload>("");

	return state;
}

const StateControl GameRequestHandler::updateGame()
{
	StateControl state;
	
	state.userInfo.username = m_user;
	state.userInfo.game = m_game;
	state.userInfo.isFirstPlayer = m_isFirstPlayer;
	state.userInfo.gameStarted = m_gameStarted;
	state.newState = gameState;

	std::string boardsStr = m_game->getBoardStr(m_isFirstPlayer, false);
	boardsStr += m_game->getBoardStr(!m_isFirstPlayer, true);

	int winner = m_game->getWinner();

	if(m_game->isActive() == true && m_gameStarted == false)
	{
		state.response = std::make_shared<VerificationPayload>("Game Started");
		state.userInfo.gameStarted = true;
	}
	else if(winner != 0)
	{
		state.newState = menuState;
		int myNumber = this->m_isFirstPlayer ? 1 : 2;
		if(winner == myNumber)
		{
			state.response = std::make_shared<VictoryPayload>(boardsStr);
			std::string sqlQuery = "UPDATE Stats SET Wins = Wins + 1 WHERE Username = '" + this->m_user + "';";
			std::string err_msg = "";
			executeSQLQuery(db, sqlQuery, err_msg);
		}
		else
		{
			state.response = std::make_shared<LossPayload>(boardsStr);
			std::string sqlQuery = "UPDATE Stats SET Losses = Games - Wins WHERE Username = '" + this->m_user + "';";
			std::string err_msg = "";
			executeSQLQuery(db, sqlQuery, err_msg);
		}

		std::string sqlQuery = "UPDATE Stats SET WinRate = CAST(Wins AS REAL) / CAST(Games AS REAL) WHERE Username = '" + this->m_user + "';";
		std::string err_msg = "";
		executeSQLQuery(db, sqlQuery, err_msg);
	}
	else if (m_gameStarted)
	{
		state.response = std::make_shared<UpdateGamePayload>(boardsStr);
	}
	else
	{
		state.response = std::make_shared<UpdateGamePayload>("");
	}

	return state;
}

const StateControl GameRequestHandler::resign() const
{
	StateControl state;
	
	state.userInfo.username = m_user;
	state.newState = menuState;

	m_game->resign(m_isFirstPlayer);

	state.response = std::make_shared<VerificationPayload>("");
	
	
	return state;
}

const StateControl GameRequestHandler::generateErrorResponse(std::string data)
{
	UserInfo userInfo;
	userInfo.username = m_user;
	userInfo.isFirstPlayer = m_isFirstPlayer;
	userInfo.game = m_game;

	return IRequestHandler::generateErrorResponse(gameState, userInfo, data);
}