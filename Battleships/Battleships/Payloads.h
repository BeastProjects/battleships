#pragma once
#include "IPayload.h"

#define VERIFICATION 22
#define ERROR 255
#define UPDATE_GAME 44
#define JOIN_GAME 100
#define CREATE_GAME 150
#define SIGNOUT 169
#define RESIGN 170
#define LOSS 33
#define VICTORY 10
#define LOGIN 50
#define SIGNUP 60
#define GAME_MOVE 200
#define SET_BOARD 5
#define START_GAME 120
#define QUIT 122


class LoginPayload  : public IPayload
{
public:
	LoginPayload(const std::string data);
	virtual unsigned int getCode() const;
};

class SignupPayload : public IPayload
{
public:
	SignupPayload(const std::string data);
	virtual unsigned int getCode() const;
};

class SignoutPayload : public IPayload
{
public:
	SignoutPayload(const std::string data);
	virtual unsigned int getCode() const;
};

class LeavePayload : public IPayload
{
public:
	LeavePayload(const std::string data);
	virtual unsigned int getCode() const;
};

class CreateGamePayload : public IPayload
{
public:
	CreateGamePayload(const std::string data);
	virtual unsigned int getCode() const;
};

class SetBoardPayload : public IPayload
{
public:
	SetBoardPayload(const std::string data);
	virtual unsigned int getCode() const;
};

class GameMovePayload : public IPayload
{
public:
	GameMovePayload(const std::string data);
	virtual unsigned int getCode() const;
};

class JoinGamePayload : public IPayload
{
public:
	JoinGamePayload(const std::string data);
	virtual unsigned int getCode() const;
};

class UpdateGamePayload : public IPayload
{
public:
	UpdateGamePayload(const std::string data);
	virtual unsigned int getCode() const;
};

class VerificationPayload : public IPayload
{
public:
	VerificationPayload(const std::string data);
	virtual unsigned int getCode() const;
};

class ErrorPayload : public IPayload
{
public:
	ErrorPayload(const std::string data);
	virtual unsigned int getCode() const;
};

class VictoryPayload : public IPayload
{
public:
	VictoryPayload(const std::string data);
	virtual unsigned int getCode() const;
};

class LossPayload : public IPayload
{
public:
	LossPayload(const std::string data);
	virtual unsigned int getCode() const;
};

class StartGamePayload : public IPayload
{
public:
	StartGamePayload(const std::string data);
	virtual unsigned int getCode() const;
};