#include <sstream>
#include "LoginRequestHandler.h"

LoginManager IRequestHandler::loginManager;
std::mutex IRequestHandler::loginMutex;

std::vector<std::string> splitString(const std::string& input, char delimiter);

LoginRequestHandler::LoginRequestHandler() {}

// Handles the client request
const StateControl LoginRequestHandler::handleRequest(IPayload_ptr request)
{
	StateControl result;

	int code = request->getCode();

	if (code != LOGIN && code != SIGNUP)
	{
		return generateErrorResponse("Error - Irrelevant request");
	}

	try
	{
		if(code == LOGIN)
		{
			result = login(request);
		}
		else
		{
			result = signup(request);
		}
	}
	catch (const std::exception& exp)
	{
		std::shared_ptr<IPayload> payload = std::make_shared<ErrorPayload>(exp.what());
		result = { loginState, payload, "", nullptr, NULL };
	}

	return result;
}

// Handles a login request
const StateControl LoginRequestHandler::login(IPayload_ptr request)
{

	StateControl result;

	std::string userInfo = request->getData();

	size_t pipePos = userInfo.find('|');

	std::string username = userInfo.substr(0, pipePos);
	std::string password = userInfo.substr(pipePos + 1);

	std::lock_guard<std::mutex> loginManagerLock(loginMutex);
	
	if (loginManager.isUserLogged(username))
	{
		throw(std::exception("User is already logged"));
	}

	else if (!(loginManager.doesPasswordMatch(username, password)))
	{
		throw(std::exception("Password or username are incorrect"));
	}
	
	loginManager.logUser(username);
	std::shared_ptr<IPayload> payload = std::make_shared<VerificationPayload>("");
	result = {menuState, payload, username, nullptr, NULL};

	return result;
}

//sima
//Handles a sign up request
const StateControl LoginRequestHandler::signup(IPayload_ptr request)
{

	char* err_msg = 0;

	StateControl result;
	std::string userInfo = request->getData();

	std::vector<std::string> msgParts = splitString(userInfo, '|');

	if (msgParts.size() != 5)
	{
		throw(std::exception("Bad format"));
	}

	std::string username = msgParts[0];
	std::string password = msgParts[1];

	std::lock_guard<std::mutex> loginManagerLock(loginMutex);

	if (loginManager.isUserLogged(username))
	{
		throw(std::exception("Username is already logged"));
	}

	if (loginManager.doesUserExist(username))
	{
		throw(std::exception("Username is already taken"));
	}

	if (username == "" || password == "")
	{
		throw(std::exception("Username and password must be entered"));
	}

	if (username.length() >= CHARACTERS_LIMIT || password.length() >= CHARACTERS_LIMIT)
	{
		throw(std::exception("Username or password are too long, character limit - 16"));
	}

	this->loginManager.addNewUser(username, password);

	std::string sqlString = "INSERT INTO Users (Username, Password, Mail, Age, Mom) VALUES ('" + username + "', '" + password + "', '" + msgParts[2] + "', '" + msgParts[3] + "', '" + msgParts[4] + "')";
	const char* sql = sqlString.c_str();
	sqlite3_exec(db, sql, 0, 0, &err_msg);

	sqlString = "INSERT INTO Stats (Username, Games, Wins, Losses, WinRate) VALUES ('" + username + "', 0, 0, 0, 0);";
	sql = sqlString.c_str();
	sqlite3_exec(db, sql, 0, 0, &err_msg);

	loginManager.logUser(username);
	std::shared_ptr<IPayload> payload = std::make_shared<VerificationPayload>("");
	result = { menuState, payload, username, nullptr, NULL};

	return result;
}


// Generates an error response for a login state
const StateControl LoginRequestHandler::generateErrorResponse(std::string data)
{
	UserInfo userInfo;
	return IRequestHandler::generateErrorResponse(loginState, userInfo, data);
}

std::vector<std::string> splitString(const std::string& input, char delimiter) {
	std::vector<std::string> tokens;
	std::stringstream ss(input);
	std::string token;
	while (std::getline(ss, token, delimiter)) {
		tokens.push_back(token);
	}
	return tokens;
}