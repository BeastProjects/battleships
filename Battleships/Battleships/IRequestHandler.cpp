#include "IRequestHandler.h"
#include <functional>

sqlite3* IRequestHandler::db;

void IRequestHandler::setDB(sqlite3* myDB)
{
    db = myDB;
}

void IRequestHandler::setUsers()
{
    int rc;

    // SQLite query to retrieve data from the Users table
    const char* sql = "SELECT Username, Password FROM Users";
    sqlite3_stmt* stmt;

    rc = sqlite3_prepare_v2(db, sql, -1, &stmt, nullptr);
    if (rc != SQLITE_OK) {
        std::cerr << "SQL error: " << sqlite3_errmsg(db) << std::endl;
        sqlite3_close(db);
    }

    // Iterate through the query results and populate the map
    while (sqlite3_step(stmt) == SQLITE_ROW) {
        const char* username = reinterpret_cast<const char*>(sqlite3_column_text(stmt, 0));
        const char* password = reinterpret_cast<const char*>(sqlite3_column_text(stmt, 1));
        loginManager.addNewUser(username, password);
    }
}

// Generates an error response to the user when his request is invalid
const StateControl IRequestHandler::generateErrorResponse(HandlerState curState, UserInfo userInfo, std::string data)
{
	std::shared_ptr<IPayload> payload = std::make_shared<ErrorPayload>(data);

	StateControl result = { curState, payload, userInfo};

	return result;
}


int IRequestHandler::executeSQLQuery(sqlite3* db, const std::string& query, std::string& error_message) {
    char* err_msg = nullptr;

    // Execute the SQL query
    int rc = sqlite3_exec(db, query.c_str(), 0, 0, &err_msg);

    if (rc != SQLITE_OK) {
        // Handle the error
        error_message = err_msg; // Store the error message
        sqlite3_free(err_msg); // Free the error message
    }

    return rc; // Return the result code (SQLITE_OK for success)
}