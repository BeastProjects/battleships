#include "Communicator.h"

#define BYTE_RANGE_OF_VALUES 256
#define DEFAULT_PORT 5000
#define HEADERS_BYTE_RANGE 3

char recvByte(SOCKET socket);

// Constructor
Communicator::Communicator()
{
	m_port = DEFAULT_PORT;

	m_serverSocket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (m_serverSocket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}

	bindAndListen();
}

Communicator::Communicator(const int port)
{
	m_port = port;

	m_serverSocket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (m_serverSocket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}

	bindAndListen();
}

// Destructor
Communicator::~Communicator()
{
	try
	{
		::closesocket(m_serverSocket);
	}
	catch (...) {}
}


// listen to connecting requests from clients
// accept them, and create thread for each client
void Communicator::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(m_port);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = 0;

	// again stepping out to the global namespace
	if (::bind(m_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - bind");
	}
	std::cout << "Binded" << std::endl;

	if (::listen(m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - listen");
	}
	std::cout << "Listening..." << std::endl;
}

// Accepts a new client and returns the socket
SOCKET Communicator::acceptNewClient()
{
	SOCKET newClientSocket = accept(m_serverSocket, NULL, NULL);
	return newClientSocket;
}

// Receives a new message
IPayload_ptr Communicator::recvMsg(SOCKET clientSocket)
{
	std::string buffer;
	int msgCode = 0, msgLen = 0;
	unsigned char byte = 0;

	// recieving the message code
	byte = recvByte(clientSocket);
	msgCode = byte;
	buffer += byte;

	// recieving the message size
	for (int i = 1; i >= 0; i--)
	{
		byte = recvByte(clientSocket);
		msgLen += byte * (int)pow(BYTE_RANGE_OF_VALUES, i);
		buffer += byte;
	}

	// recieving the message
	for (int i = 0; i < msgLen; i++)
	{
		byte = recvByte(clientSocket);
		buffer += byte;
	}

	return analyzeMsg(buffer);
}

// Analyzes a new msg from the client
IPayload_ptr Communicator::analyzeMsg(std::string request) const
{
	IPayload_ptr msgPayload;
	unsigned char msgCode = static_cast<unsigned char>(request[0]);
	std::string data = request.substr(HEADERS_BYTE_RANGE);

	switch (msgCode)
	{
	case LOGIN:
		msgPayload = std::make_shared<LoginPayload>(data);
		break;

	case SIGNUP:
		msgPayload = std::make_shared<SignupPayload>(data);
		break;

	case SIGNOUT:
		msgPayload = std::make_shared<SignoutPayload>(data);
		break;

	case ERROR:
		msgPayload = std::make_shared<ErrorPayload>(data);
		break;

	case VERIFICATION:
		msgPayload = std::make_shared<VerificationPayload>(data);
		break;

	case UPDATE_GAME:
		msgPayload = std::make_shared<UpdateGamePayload>(data);
		break;

	case CREATE_GAME:
		msgPayload = std::make_shared<CreateGamePayload>(data);
		break;

	case JOIN_GAME:
		msgPayload = std::make_shared<JoinGamePayload>(data);
		break;

	case LOSS:
		msgPayload = std::make_shared<LossPayload>(data);
		break;

	case VICTORY:
		msgPayload = std::make_shared<VictoryPayload>(data);
		break;

	case GAME_MOVE:
		msgPayload = std::make_shared<GameMovePayload>(data);
		break;

	case RESIGN:
		msgPayload = std::make_shared<LeavePayload>(data);
		break;

	case START_GAME:
		msgPayload = std::make_shared<StartGamePayload>(data);
		break;
	

	default:
		break;
	}

	return msgPayload;
}

// Gets a response to send the client and sends it
void Communicator::sendMsg(SOCKET clientSocket, IPayload_ptr response)
{
	std::string msgToClient = packMsg(response);
	encryptOrDecrypt(&msgToClient);

	if (send(clientSocket, msgToClient.c_str(), (int)msgToClient.size(), 0) == SOCKET_ERROR)
	{
		throw(std::exception("Socket Error"));
	}
}

// Packs a response
std::string Communicator::packMsg(IPayload_ptr response) const
{
	std::string msg = "";
	int dataLen = (int)response->getData().size();

	// adding the message code
	msg += response->getCode();

	// adding the data length
	msg += dataLen / BYTE_RANGE_OF_VALUES;
	msg += dataLen % BYTE_RANGE_OF_VALUES;

	// adding the data
	msg += response->getData();

	return msg;
}

char recvByte(SOCKET socket)
{
	char byte = 0;
	int result = recv(socket, &byte, 1, 0);

	if (result == SOCKET_ERROR || static_cast<unsigned char>(byte) == 204)
	{
		throw(std::exception("Socket Error"));
	}

	return BYTE_RANGE_OF_VALUES - 1 - byte;
}

void Communicator::encryptOrDecrypt(std::string* msg)
{
	for (int i = 0; i < msg->size(); i++)
	{
		(*msg)[i] = BYTE_RANGE_OF_VALUES - 1 - (*msg)[i];
	}
}