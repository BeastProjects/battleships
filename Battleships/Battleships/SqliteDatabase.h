#pragma once
#include <iostream>
#include "sqlite3.h"

class SqliteDatabase
{
public:
	bool open();
	bool close();

	int doesUserExist(std::string username);
	int doesPasswordMatch(std::string username, std::string password);
	int addNewUser(std::string username, std::string password);

	/*
	float getPlayerAverageAnswerTime(std::string username) override;
	int getNumOfCorrectAnswers(std::string username) override;
	int getNumOfTotalAnswers(std::string username) override;
	int getNumOfPlayerGames(std::string username) override;
	int getPlayerScore(std::string username) override;
	std::vector<std::string> getHighScores() override;

	int submitGameStatistics(GameData gameData, std::string username) override;
	*/

private:
	// Fields
	sqlite3* m_db;

	// Callback functions 
	static int user_callback(void* data, int argc, char** argv, char** azColName);
	//static int question_callback(void* data, int argc, char** argv, char** azColName);
	static int statistics_callback(void* data, int argc, char** argv, char** azColName);


};