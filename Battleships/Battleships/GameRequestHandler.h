#pragma once
#include <string>
#include "Game.h"
#include "IRequestHandler.h"

class GameRequestHandler : public IRequestHandler
{
public:
	GameRequestHandler(UserInfo userData);
	const virtual StateControl handleRequest(IPayload_ptr request);

private:

	virtual const StateControl generateErrorResponse(std::string data) override;

	const StateControl gameMove(IPayload_ptr request);
	const StateControl updateGame();
	const StateControl resign() const;

	bool m_gameStarted;
	bool m_isFirstPlayer;
	Game* m_game;
	std::string m_user;

};