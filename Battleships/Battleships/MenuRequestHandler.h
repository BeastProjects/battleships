#pragma once
#include <map>
#include <random>
#include "IRequestHandler.h"

class MenuRequestHandler : public IRequestHandler
{
public:

	// Constructor
	MenuRequestHandler(std::string username);
	
	void removeUser();
	const virtual StateControl handleRequest(IPayload_ptr request);

private:

	const StateControl createGame(IPayload_ptr request);
	const StateControl joinGame(IPayload_ptr request);
	const StateControl signout(IPayload_ptr request);

	unsigned int generateRandomNumber();

	virtual const StateControl generateErrorResponse(std::string data) override;

	static std::map<unsigned int, Game> m_games;
	std::string m_user;

};