#pragma once
#include <iostream>
#include <string>

class IPayload
{
public:
	IPayload(const std::string data);

	std::string getData() const;

	virtual unsigned int getCode() const;

private:
	std::string m_data;
};

typedef std::shared_ptr<IPayload> IPayload_ptr;