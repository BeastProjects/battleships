#include "Server.h"
#include <thread>

sqlite3* Server::m_db = nullptr;

#define PORT 8888

Server::Server() : m_communicator(PORT), m_clients(), m_terminateFlag(false) {}

Server::~Server()
{
	sqlite3_close(this->m_db);
}

void Server::startHandleRequests()
{
	char* err_msg = 0;

	int rc = sqlite3_open("battleshipDB.db", &this->m_db);

	sqlite3_busy_timeout(this->m_db, 1000);

	if (rc != SQLITE_OK) {

		sqlite3_close(this->m_db);
	}

	IRequestHandler::setDB(m_db);
	IRequestHandler::setUsers();

	try
	{
		while (true)
		{
			SOCKET newClient = m_communicator.acceptNewClient();
			m_clients.insert({ newClient, CreateLoginRequestHandler() });
			std::thread tr(&Server::handleNewClient, this, newClient);
			tr.detach();
		}
	}
	catch (const std::exception&)
	{
		sqlite3_close(this->m_db);
	}

	

}

void Server::handleNewClient(SOCKET clientSocket)
{
	try
	{
		while (!m_terminateFlag)
		{
			IRequestHandler_ptr requestHandler = m_clients.at(clientSocket);
			IPayload_ptr request = m_communicator.recvMsg(clientSocket);

			if(request->getData() == "QUIT")
			{
				WSACleanup();
				m_terminateFlag = true;
				break;
			}

			StateControl response = requestHandler->handleRequest(request);
			m_clients.at(clientSocket) = generateNewRequestHandler(response);
			m_communicator.sendMsg(clientSocket, response.response);
		}
	}
	catch (const std::exception& exp)
	{
		std::cout << exp.what() << std::endl;
		IRequestHandler_ptr requestHandler = m_clients.at(clientSocket);
		
		try
		{
			MenuRequestHandler* handler = static_cast<MenuRequestHandler*>(requestHandler.get());
			handler->removeUser();
		}
		catch (const std::exception&)
		{

		}
	}

	
}

// Gets the new state for the next request and returns a new RequestHandler
IRequestHandler_ptr Server::generateNewRequestHandler(StateControl stats)
{
	IRequestHandler_ptr requestHandler;

	if (stats.newState == loginState)
	{
		requestHandler = CreateLoginRequestHandler();
	}
	else if(stats.newState == menuState)
	{
		requestHandler = CreateMenuRequestHandler(stats.userInfo.username);
	}
	else if (stats.newState == gameState)
	{
		requestHandler = CreateGameRequestHandler(stats.userInfo.username, stats.userInfo.isFirstPlayer, stats.userInfo.gameStarted, stats.userInfo.game);
	}

	return requestHandler;
}

// Creates a new LoginRequestHandler
IRequestHandler_ptr Server::CreateLoginRequestHandler()
{
	IRequestHandler_ptr handler = std::make_shared<LoginRequestHandler>();
	return handler;
}

// Creates a new MenuRequestHandler
IRequestHandler_ptr Server::CreateMenuRequestHandler(std::string username)
{
	IRequestHandler_ptr handler = std::make_shared<MenuRequestHandler>(username);
	return handler;
}

// Creates a new GameRequestHandler
IRequestHandler_ptr Server::CreateGameRequestHandler(std::string username, char color, bool gameStarted, Game* game)
{
	UserInfo userInfo = { username, game, gameStarted, color };
	IRequestHandler_ptr handler = std::make_shared<GameRequestHandler>(userInfo);
	return handler;
}
