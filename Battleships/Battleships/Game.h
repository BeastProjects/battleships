#pragma once
#include <utility>
#include <vector>
#include <string>

#define SIZE 10

const int NUM_SHIPS = 5;
const int BOARD_SIZE = 10;
const char EMPTY = '0';
const char SHIP = 'B';
const char HIT = 'X';
const char MISS = '.';

typedef unsigned int coordinate;
typedef std::pair<coordinate, coordinate> square;
typedef std::vector<std::vector<char>> board;

class Game
{
public:
	Game();
	const int getWinner() const;
	const char makeMove(square move); // returns the result of the move
	const char getTurn() const;
	const bool isActive() const;
	const void activateGame();
	const void resign(bool isFirstOpponent);
	const std::string getBoardStr(bool isFirstBoard, bool hideShips) const;


private:
	bool m_active;
	bool m_turn;
	int m_resignedOpponent;
	std::vector<std::vector<char>> m_boardP1;
	std::vector<std::vector<char>> m_boardP2;

	//Useful functions for utility
	const void setBoards();
	const char decideWin(bool firstWins, bool secondWins) const;
	const void printBoard() const;
	const void changeTurn();
	bool isValidPlacement(const board& board, int row, int col, int shipSize, bool horizontal);
	void generateBoard(board& board);

};