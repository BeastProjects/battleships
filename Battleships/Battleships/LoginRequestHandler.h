#pragma once
#include "IRequestHandler.h"

class LoginRequestHandler : public IRequestHandler
{
public:

	// Constructor
	LoginRequestHandler();

	const virtual StateControl handleRequest(IPayload_ptr request);

private:

	const StateControl login(IPayload_ptr request);
	const StateControl signup(IPayload_ptr request);

	virtual const StateControl generateErrorResponse(std::string data) override;

};