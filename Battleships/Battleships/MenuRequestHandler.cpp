#include "MenuRequestHandler.h"

std::map<unsigned int, Game> MenuRequestHandler::m_games;

MenuRequestHandler::MenuRequestHandler(std::string username) : m_user(username) {}

bool areAllDigits(const std::string& str) {
	for (char c : str) {
		if (!std::isdigit(c)) {
			return false;
		}
	}
	return true;
}

void MenuRequestHandler::removeUser()
{
	std::lock_guard<std::mutex> locker(loginMutex);
	loginManager.removeUser(m_user);
}

const StateControl MenuRequestHandler::handleRequest(IPayload_ptr request)
{
	StateControl result;

	unsigned int code = request->getCode();

	try
	{
		switch (code)
		{
			case CREATE_GAME:
				result = this->createGame(request);
				break;

			case JOIN_GAME:
				result = this->joinGame(request);
				break;

			case SIGNOUT:
				result = this->signout(request);
				break;

			default:
				result = generateErrorResponse("Irrelevant Menu Request");
		}
	}
	catch (const std::exception& exp)
	{
		std::shared_ptr<IPayload> payload = std::make_shared<ErrorPayload>(exp.what());
		result = { menuState, payload, "", nullptr };
	}

	return result;
}

// Handles a CreateGame request
const StateControl MenuRequestHandler::createGame(IPayload_ptr request)
{
	StateControl state;
	
	Game newGame;
	std::string firstBoard = newGame.getBoardStr(true, false); // First player's board
	std::string secondBoard = newGame.getBoardStr(false, false); // Second player's board

	unsigned int gameCode = generateRandomNumber();

	m_games.insert({ gameCode, newGame });

	IPayload_ptr response = std::make_shared<VerificationPayload>(std::to_string(gameCode));
	
	UserInfo userInfo = { this->m_user, &m_games[gameCode], false, true };

	std::string sqlQuery = "UPDATE Stats SET Games = Games + 1 WHERE Username = '" + this->m_user + "';";
	std::string err_msg = "";
	executeSQLQuery(db, sqlQuery, err_msg);

	sqlQuery = "INSERT INTO BoardHistory (BoardP1, BoardP2) VALUES ('" + firstBoard + "', '" + secondBoard + "');";
	executeSQLQuery(db, sqlQuery, err_msg);

	state.newState = gameState;
	state.response = response;
	state.userInfo = userInfo;

	return state;
}

// Handles a JoinGame request
const StateControl MenuRequestHandler::joinGame(IPayload_ptr request)
{
	if (!areAllDigits(request->getData()) || request->getData() == "")
	{
		return generateErrorResponse("Numbers only!!!");
	}

	unsigned int gameCode = std::stoi(request->getData());
	auto codeToGame = m_games.find(gameCode);
	
	if (codeToGame == m_games.end())
	{
		return generateErrorResponse("Error - No game found");
	}

	else if (codeToGame->second.isActive())
	{
		return generateErrorResponse("Error - Game is already active");
	}

	codeToGame->second.activateGame();
	StateControl state;
	UserInfo userInfo = { this->m_user, &m_games[gameCode], true, false };

	std::string sqlQuery = "UPDATE Stats SET Games = Games + 1 WHERE Username = '" + this->m_user + "';";
	std::string err_msg = "";
	executeSQLQuery(db, sqlQuery, err_msg);

	state.response = std::make_shared<VerificationPayload>("Joined game");
	state.newState = gameState;
	state.userInfo = userInfo;

	return state;

}

// Handles a Signout request
const StateControl MenuRequestHandler::signout(IPayload_ptr request)
{
	StateControl state;

	state.newState = loginState;
	state.response = std::make_shared<VerificationPayload>("signed out");

	removeUser();
	
	return state;
}


unsigned int MenuRequestHandler::generateRandomNumber()
{
	std::random_device rd;
	std::mt19937 gen(rd());


	std::uniform_int_distribution<int> distribution(1000, 9999);

	return distribution(gen);
}

// Generates an error response for a menu state
const StateControl MenuRequestHandler::generateErrorResponse(std::string data)
{
	UserInfo userInfo;
	userInfo.username = m_user;

	return IRequestHandler::generateErrorResponse(menuState, userInfo, data);
}