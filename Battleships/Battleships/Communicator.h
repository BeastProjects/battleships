#pragma once
#include "Payloads.h"
#include <memory>
#include <WinSock2.h>
#include <Windows.h>

class Communicator
{
public:

	// Constructor
	Communicator();
	Communicator(const int port);
	~Communicator();

	// Methods
	SOCKET acceptNewClient();
	void sendMsg(SOCKET clientSocket, IPayload_ptr response);
	IPayload_ptr recvMsg(SOCKET clientSocket);


private:

	// Fields
	unsigned int m_port;
	SOCKET m_serverSocket;

	// Methods
	void bindAndListen();
	std::string packMsg(IPayload_ptr response) const;
	IPayload_ptr analyzeMsg(std::string request) const;
	void encryptOrDecrypt(std::string* msg);
};