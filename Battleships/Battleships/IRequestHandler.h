#pragma once
#include <string>
#include <mutex>
#include "Game.h"
#include "Payloads.h"
#include "LoginManager.h"
#include "sqlite3.h"

enum HandlerState {loginState, menuState, gameState};

typedef struct UserInfo
{
	std::string username;
	Game* game;
	bool gameStarted;
	bool isFirstPlayer;
}UserInfo;

typedef struct StateControl 
{
	HandlerState newState;
	std::shared_ptr<IPayload> response;
	UserInfo userInfo;
}StateControl;

class IRequestHandler
{
public:
	const virtual StateControl handleRequest(IPayload_ptr request) = 0;
	static void setDB(sqlite3* myDB);
	static void setUsers();
	static int executeSQLQuery(sqlite3* db, const std::string& query, std::string& error_message);

protected:
	static LoginManager loginManager;
	static std::mutex loginMutex;
	const StateControl generateErrorResponse(HandlerState curState, UserInfo userInfo, std::string data);
	virtual const StateControl generateErrorResponse(std::string data) = 0;
	static sqlite3* db;
};

typedef std::shared_ptr<IRequestHandler> IRequestHandler_ptr;