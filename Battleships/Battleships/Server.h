#pragma once
#include "Communicator.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "GameRequestHandler.h"
#include <map>
#include "sqlite3.h"


class Server
{
public:

	// Constructor
	Server();

	// Destructor
	~Server();

	void startHandleRequests();

private:

	// Fields
	Communicator m_communicator;
	std::map<SOCKET, IRequestHandler_ptr> m_clients;
	std::atomic<bool> m_terminateFlag;
	static sqlite3* m_db;

	void handleNewClient(SOCKET clientSocket);
	IRequestHandler_ptr generateNewRequestHandler(StateControl stats);
	IRequestHandler_ptr CreateLoginRequestHandler();
	IRequestHandler_ptr CreateMenuRequestHandler(std::string username);
	IRequestHandler_ptr CreateGameRequestHandler(std::string username, char color, bool gameStarted, Game* game);
};