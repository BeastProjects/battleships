#include "Game.h"
#include <iostream>
#include <math.h>
#include <ctime>
#include <cstdlib>

void copy2DCharArray(char dest[][SIZE], const char src[][SIZE]);

Game::Game()
{
    setBoards();

    this->m_turn = true;
    this->m_active = false;
    this->m_resignedOpponent = 0; //0 - for none
}

const char Game::makeMove(square move)
{
    board* bombedBoard = m_turn ? &m_boardP2 : &m_boardP1;

    if (move.first >= SIZE || move.first < 0 || move.second >= SIZE || move.second < 0)
    {
        return 0;
    }

    char squareSign = (*bombedBoard)[move.second][move.first];

    if (squareSign == SHIP)
    {
        (*bombedBoard)[move.second][move.first] = HIT;
        return HIT;
    }

    if (squareSign == EMPTY)
    {
        (*bombedBoard)[move.second][move.first] = MISS;
        changeTurn();
        return MISS;
    }

    return 0;
}

//Tracks all over the board and returns the winner with the decideWin function as a char
const int Game::getWinner() const
{
    int firstBombs = 0;
    int secondBombs = 0;
    const int MAXHITS = 17;

    for (int y = 0; y < SIZE; y++)
    {
        for (int x = 0; x < SIZE; x++)
        {
            if (m_boardP1[y][x] == HIT)
            {
                firstBombs++;
            }
            else if (m_boardP2[y][x] == HIT)
            {
                secondBombs++;
            }

        }
    }

    if (firstBombs == MAXHITS || m_resignedOpponent == 1)
    {
        return 2;
    }
    else if (secondBombs == MAXHITS || m_resignedOpponent == 2)
    {
        return 1;
    }

    return 0;
}

const void Game::setBoards()
{
    std::srand(static_cast<unsigned int>(std::time(nullptr)));
    generateBoard(m_boardP1);
    generateBoard(m_boardP2);
}

const char Game::getTurn() const
{
    return this->m_turn;
}

const bool Game::isActive() const
{
    return this->m_active;
}

const void Game::resign(bool isFirstOpponent) 
{
    this->m_resignedOpponent = isFirstOpponent ? 1 : 2;
}

const void Game::activateGame()
{
    this->m_active = true;
}

const std::string Game::getBoardStr(bool isFirstBoard, bool hideShips) const
{
    std::string boardStr = "";
    board chosenBoard = isFirstBoard ? m_boardP1 : m_boardP2;

    for (int y = 0; y < SIZE; y++)
    {
        for (int x = 0; x < SIZE; x++)
        {
            if (hideShips && chosenBoard[y][x] == SHIP)
            {
                boardStr += EMPTY;
            }
            else
            {
                boardStr += chosenBoard[y][x];
            }
        }
    }

    return boardStr;
}

//Returns a winner as a char
const char Game::decideWin(bool firstWins, bool secondWins) const
{

    if (firstWins || this->m_resignedOpponent == 2)
    {
        return 1;
    }
    else if (secondWins || this->m_resignedOpponent == 1)
    {
        return 2;
    }
    else
    {
        return 0; // None of them are winning
    }
}

//Prints out the board
const void Game::printBoard() const
{
    for (int row = 0; row < SIZE; row++)
    {
        for (int col = 0; col < SIZE; col++)
        {
            if(m_boardP1[row][col] == 0)
            {
                std::cout << "0" << ' ';
            }
            else
            {
                std::cout << m_boardP1[row][col] << ' ';
            }
        }
        std::cout << '\n';
    }

    std::cout << '\n';

    for (int row = 0; row < SIZE; row++)
    {
        for (int col = 0; col < SIZE; col++)
        {
            if (m_boardP2[row][col] == 0)
            {
                std::cout << "0" << ' ';
            }
            else
            {
                std::cout << m_boardP2[row][col] << ' ';
            }
        }
        std::cout << '\n';
    }
}

//Changes the turn after making a move
const void Game::changeTurn()
{
    m_turn = !m_turn;
}

bool Game::isValidPlacement(const board& board, int row, int col, int shipSize, bool horizontal)
{
    if (row < 0 || row >= BOARD_SIZE || col < 0 || col >= BOARD_SIZE)
    {
        return false; // Check if the placement is within the board boundaries
    }

    if (horizontal)
    {
        if (col + shipSize > BOARD_SIZE)
        {
            return false; // Check if the ship goes out of bounds horizontally
        }

        for (int i = col; i < col + shipSize; ++i)
        {
            if (board[row][i] != EMPTY)
            {
                return false; // Check if there's a ship already in the way
            }
        }
    }
    else
    {
        if (row + shipSize > BOARD_SIZE)
        {
            return false; // Check if the ship goes out of bounds vertically
        }

        for (int i = row; i < row + shipSize; ++i)
        {
            if (board[i][col] != EMPTY) {
                return false; // Check if there's a ship already in the way
            }
        }
    }

    // Check if the ship is placed too close to another ship
    for (int i = std::max(0, row - 1); i < std::min(BOARD_SIZE, row + shipSize + 1); ++i)
    {
        for (int j = std::max(0, col - 1); j < std::min(BOARD_SIZE, col + shipSize + 1); ++j)
        {
            if (board[i][j] != EMPTY)
            {
                return false;
            }
        }
    }

    return true;
}

void Game::generateBoard(board& board)
{
    board = std::vector<std::vector<char>>(BOARD_SIZE, std::vector<char>(BOARD_SIZE, EMPTY));


    // Define the number and size of ships
    const int numShips = 5;
    const std::vector<int> shipSizes = { 5, 4, 3, 3, 2 };

    for (int shipIndex = 0; shipIndex < numShips; ++shipIndex)
    {
        int shipSize = shipSizes[shipIndex];
        bool placed = false;

        while (!placed)
        {
            bool horizontal = std::rand() % 2 == 0;
            int row = std::rand() % BOARD_SIZE;
            int col = std::rand() % BOARD_SIZE;

            if (isValidPlacement(board, row, col, shipSize, horizontal))
            {
                if (horizontal)
                {
                    for (int i = col; i < col + shipSize; ++i)
                    {
                        board[row][i] = SHIP;
                    }
                }
                else
                {
                    for (int i = row; i < row + shipSize; ++i)
                    {
                        board[i][col] = SHIP;
                    }
                }
                placed = true;
            }
        }
    }
}

void copy2DCharArray(char dest[][SIZE], const char src[][SIZE]) 
{
    for (int i = 0; i < SIZE; i++) 
    {
        for (int j = 0; j < SIZE; j++) 
        {
            dest[i][j] = src[i][j];
        }
    }
}