#pragma comment(lib, "Ws2_32.lib")
#include "Server.h"
#include "WSAInitializer.h"

int main()
{
	WSAInitializer wsa_init;
	Server server;
	server.startHandleRequests();

	return 0;
}