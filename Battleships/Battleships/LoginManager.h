#pragma once
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <map>

#define CHARACTERS_LIMIT 16

class LoginManager
{
public:

	LoginManager();

	const bool addNewUser(std::string username, std::string password);
	const bool isUserLogged(std::string username) const;
	const bool doesPasswordMatch(std::string username, std::string password) const;
	const bool doesUserExist(std::string username) const;
	void logUser(std::string username);
	void removeUser(std::string username);

private:
	std::map<std::string, std::string> m_users;
	std::vector<std::string> m_loggedUsers;

};