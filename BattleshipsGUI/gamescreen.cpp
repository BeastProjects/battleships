#include "gamescreen.h"
#include "ui_gamescreen.h"

GameScreen::GameScreen(QWidget *parent, bool playingFirst, std::string numCode) :
    QDialog(parent),
    ui(new Ui::GameScreen)
{
    ui->setupUi(this);
    m_gameEnded = false;
    m_gameStarted = !playingFirst;
    m_playerBoard = GameBoard(this, 50, 100, 250, true);
    m_enemyBoard = GameBoard(this, 400, 100, 250, false);
    m_updateTimer= new QTimer(this);

    QFont font;
    font.setPointSize(22);
    label = new QLabel(numCode.c_str(), this); // Set the parent as the main window
    label->move(350, 400);
    label->setFont(font);
    label->setAlignment(Qt::AlignCenter);       // Center-align the text

    lblMyBoard = new QLabel("YOUR BOARD", this);
    lblMyBoard->move(50, 50);
    lblMyBoard->setFont(font);

    lblEnemyBoard = new QLabel("ENEMY'S BOARD", this);
    lblEnemyBoard->move(400, 50);
    lblEnemyBoard->setFont(font);

    // Connect the timeout signal of the timer to a slot function
    connect(m_updateTimer, &QTimer::timeout, this, &GameScreen::updateGame);

    m_updateTimer->start(1000);
}

// Used to send move requests
void GameScreen::mousePressEvent(QMouseEvent *event)
{
    if (m_gameEnded)
    {
        return;
    }

    std::pair<int, int> selectedSquare = m_enemyBoard.getBoardPos(event->x(), event->y());

    if (selectedSquare.first == -1)
    {
        return;
    }

    else
    {
        std::string msg = "";
        msg += selectedSquare.first;
        msg += selectedSquare.second;

        IPayload_ptr request = std::make_shared<GameMovePayload>(msg);
        IPayload_ptr response = Communicator::sendAndRecv(request);
    }
}

void GameScreen::handleMoveResponse(IPayload_ptr response, std::pair<int, int> square)
{
    if (response->getCode() == VERIFICATION_CODE)
    {
        if(response->getData() == "X")
        {
            m_enemyBoard.markHit(square.first, square.second);
        }

        else if(response->getData() == "O")
        {
            m_enemyBoard.markMiss(square.first, square.second);
        }
    }
}

void GameScreen::updateGame()
{

    IPayload_ptr request = std::make_shared<UpdateGamePayload>("");
    IPayload_ptr response = Communicator::sendAndRecv(request);

    int responseCode = response->getCode();

    if (m_gameStarted)
    {
        std::string responseData = response->getData();

        if (responseCode == UPDATE_GAME_CODE && responseData.size() != 0)
        {
            this->m_playerBoard.setBoard(responseData.substr(0, gridSize * gridSize));
            this->m_enemyBoard.setBoard(responseData.substr(gridSize * gridSize));
        }

        else if (responseCode == VICTORY_CODE)
        {
            this->m_playerBoard.setBoard(responseData.substr(0, gridSize * gridSize));
            this->m_enemyBoard.setBoard(responseData.substr(gridSize * gridSize));
            QMessageBox messageBox;
            messageBox.setText("YOU WON!!!");
            messageBox.setStyleSheet("QMessageBox { color: red; }");
            messageBox.exec();
            m_updateTimer->stop();
        }

        else if (responseCode == LOSS_CODE)
        {
            this->m_playerBoard.setBoard(responseData.substr(0, gridSize * gridSize));
            this->m_enemyBoard.setBoard(responseData.substr(gridSize * gridSize));
            QMessageBox messageBox;
            messageBox.setText("YOU LOST...");
            messageBox.setStyleSheet("QMessageBox { color: red; }");
            messageBox.exec();
            m_updateTimer->stop();
        }
    }

    else
    {
        if (responseCode == VERIFICATION_CODE)
        {
            label->clear();
            m_gameStarted = true;
        }
    }


}

GameScreen::~GameScreen()
{
    delete ui;
}

void GameScreen::signOut(std::string data)
{

    IPayload_ptr request = std::make_shared<SignoutPayload>(data);
    IPayload_ptr response = Communicator::sendAndRecv(request);

    if (response->getCode() == VERIFICATION_CODE)
    {
        this->close();
        this->parentWidget()->show();
    }
    else if (response->getCode() == ERROR_CODE)
    {
        qDebug() << "Failed to sign out new game";
    }
}


void GameScreen::on_GameScreen_finished(int result)
{
    qDebug() << result;
    //signOut("QUIT");
}

/*
void GameScreen::on_btnLeave_clicked()
{
    if (!m_gameEnded)
    {
        IPayload_ptr request = std::make_shared<LeavePayload>("");
        IPayload_ptr response = Communicator::sendAndRecv(request);
    }

    this->close();
    this->parentWidget()->show();
}

*/
