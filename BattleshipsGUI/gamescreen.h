#ifndef GAMESCREEN_H
#define GAMESCREEN_H

#include <QDialog>
#include "gameboard.h"
#include "communicator.h"
#include "payloads.h"

namespace Ui {
class GameScreen;
}

class GameScreen : public QDialog
{
    Q_OBJECT

public:
    explicit GameScreen(QWidget *parent = nullptr, bool playingFirst = true, std::string numCode = "");
    ~GameScreen();

protected:
    void mousePressEvent(QMouseEvent *event) override;

private slots:
    void on_GameScreen_finished(int result);

private:
    Ui::GameScreen* ui;
    GameBoard m_playerBoard;
    GameBoard m_enemyBoard;

    QTimer* m_updateTimer;
    bool m_myTurn;
    bool m_gameStarted;
    bool m_gameEnded;

    void handleMoveResponse(IPayload_ptr response, std::pair<int, int> square);
    void updateGame();
    void playerLost();
    void playerWon();
    void signOut(std::string data);

    QLabel* label;
    QLabel* lblMyBoard;
    QLabel* lblEnemyBoard;


};

#endif // GAMESCREEN_H
