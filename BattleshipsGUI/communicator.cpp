#include "communicator.h"
#include <exception>

#define BYTE_RANGE_OF_VALUES 256
#define HEADERS_BYTE_RANGE 3

SOCKET Communicator::mySocket;
std::mutex Communicator::socketMut;

char recvByte(SOCKET socket);

// Connects to the server
void Communicator::connectServer(int port, std::string ip)
{
    // Initialize Winsock
    WSADATA wsaData;
    if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
    {
        qDebug() << "WSAStartup Failed";
    }

    // Create a socket
    mySocket = socket(AF_INET, SOCK_STREAM, 0);
    if (mySocket == INVALID_SOCKET)
    {
        qDebug() << "Failed to create socket";
        WSACleanup();
        //return 1;
    }

    // Connect to the server
    sockaddr_in serverAddress;
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = inet_addr(ip.c_str()); // Change the IP address as needed
    serverAddress.sin_port = htons(port); // Change the port number as needed

    if (connect(mySocket, (struct sockaddr*)&serverAddress, sizeof(serverAddress)) != 0)
    {
        qDebug() << "Failed to connect to server";
        closesocket(mySocket);
        WSACleanup();
        //return 1;
    }
}

// Sends a message and returns the server response
IPayload_ptr Communicator::sendAndRecv(IPayload_ptr payload)
{
    std::lock_guard<std::mutex> socketLocker(socketMut);
    sendMsg(payload);
    return recvMsg();
}

// Sends message to the server
void Communicator::sendMsg(IPayload_ptr request)
{
    std::string msgToClient = packMsg(request);
    encryptOrDecrypt(&msgToClient);

    if (send(mySocket, msgToClient.c_str(), (int)msgToClient.size(), 0) == SOCKET_ERROR)
    {
        throw(std::exception());
    }
}

// Gets a payload and sets a string msg
std::string Communicator::packMsg(IPayload_ptr request)
{
    std::string msg = "";
    int dataLen = (int)request->getData().size();

    // adding the message code
    msg += request->getCode();

    // adding the data length
    msg += dataLen / BYTE_RANGE_OF_VALUES;
    msg += dataLen % BYTE_RANGE_OF_VALUES;

    // adding the data
    msg += request->getData();

    return msg;
}

// Recieves a message from the server
IPayload_ptr Communicator::recvMsg()
{
    std::string buffer;
    int msgCode = 0, msgLen = 0;
    unsigned char byte = 0;

    // recieving the message code
    byte = recvByte(mySocket);
    msgCode = byte;
    buffer += byte;

    // recieving the message size
    for (int i = 1; i >= 0; i--)
    {
        byte = recvByte(mySocket);
        msgLen += byte * (int)pow(BYTE_RANGE_OF_VALUES, i);
        buffer += byte;
    }

    // recieving the message
    for (int i = 0; i < msgLen; i++)
    {
        byte = recvByte(mySocket);
        buffer += byte;
    }

    return analyzeMsg(buffer);
}

// Gets a string msg from the
IPayload_ptr Communicator::analyzeMsg(std::string response)
{
    IPayload_ptr msgPayload;
    unsigned char msgCode = static_cast<unsigned char>(response[0]);
    std::string data = response.substr(HEADERS_BYTE_RANGE);

    switch (msgCode)
    {
    case LOGIN_CODE:
        msgPayload = std::make_shared<LoginPayload>(data);
        break;

    case SIGNOUT_CODE:
        msgPayload = std::make_shared<SignoutPayload>(data);
        break;

    case ERROR_CODE:
        msgPayload = std::make_shared<ErrorPayload>(data);
        break;

    case VERIFICATION_CODE:
        msgPayload = std::make_shared<VerificationPayload>(data);
        break;

    case UPDATE_GAME_CODE:
        msgPayload = std::make_shared<UpdateGamePayload>(data);
        break;

    case CREATE_GAME_CODE:
        msgPayload = std::make_shared<CreateGamePayload>(data);
        break;

    case JOIN_GAME_CODE:
        msgPayload = std::make_shared<JoinGamePayload>(data);
        break;

    case LOSS_CODE:
        msgPayload = std::make_shared<LossPayload>(data);
        break;

    case VICTORY_CODE:
        msgPayload = std::make_shared<VictoryPayload>(data);
        break;

    case GAME_MOVE_CODE:
        msgPayload = std::make_shared<GameMovePayload>(data);
        break;

    case LEAVE_CODE:
        msgPayload = std::make_shared<LeavePayload>(data);
        break;

    case START_GAME_CODE:
        msgPayload = std::make_shared<StartGamePayload>(data);
        break;

    default:
        break;
    }

    return msgPayload;
}

char recvByte(SOCKET socket)
{
    char byte = 0;
    int result = recv(socket, &byte, 1, 0);

    if (result == SOCKET_ERROR)
    {
        throw(std::exception());
    }

    return BYTE_RANGE_OF_VALUES - 1 - byte;
}

void Communicator::encryptOrDecrypt(std::string* msg)
{
    for (int i = 0; i < msg->size(); i++)
    {
        (*msg)[i] = BYTE_RANGE_OF_VALUES - 1 - (*msg)[i];
    }
}

