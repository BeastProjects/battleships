#pragma comment(lib, "Ws2_32.lib")
#include <iostream>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <QDebug>
#include "gamescreen.h"
#include "payloads.h"
#include "loginscreen.h"
#include "communicator.h"
#include <QApplication>

#define BYTE_RANGE_OF_VALUES 256

//std::string packMsg(IPayload* response);

int main(int argc, char *argv[])
{

    QApplication a(argc, argv);

    Communicator::connectServer(8888, "127.0.0.1");
    LoginScreen loginScreen;
    loginScreen.show();

    return a.exec();
}
