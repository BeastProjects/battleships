#include "ipayload.h"

// Constructor
IPayload::IPayload(const std::string data)
{
    this->m_data = data;
}

// Returns the data
std::string IPayload::getData() const
{
    return m_data;
}
