#ifndef MAINMENUSCREEN_H
#define MAINMENUSCREEN_H

#include <QDialog>
#include <QPushButton>
#include <QTextEdit>
#include "payloads.h"
#include "communicator.h"
#include "gamescreen.h"

namespace Ui {
class MainMenuScreen;
}

class MainMenuScreen : public QDialog
{
    Q_OBJECT

public:
    explicit MainMenuScreen(QWidget *parent = nullptr);
    ~MainMenuScreen();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_MainMenuScreen_finished(int result);

private:
    Ui::MainMenuScreen *ui;

    QPushButton* btnCreateGame;
    QPushButton* btnJoinGame;
    QPushButton* btnSignOut;

    QTextEdit* txbGameCode;

    void createGame();
    void joinGame();
    void signOut(std::string data);
};

#endif // MAINMENUSCREEN_H
