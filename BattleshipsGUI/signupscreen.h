#ifndef SIGNUPSCREEN_H
#define SIGNUPSCREEN_H

#include <QDialog>
#include "mainmenuscreen.h"
#include "communicator.h"

namespace Ui {
class SignupScreen;
}

class SignupScreen : public QDialog
{
    Q_OBJECT

public:
    explicit SignupScreen(QWidget *parent = nullptr);
    ~SignupScreen();

private slots:
    void on_btnReturn_clicked();

    void on_btnSignup_clicked();

private:
    Ui::SignupScreen *ui;
};

#endif // SIGNUPSCREEN_H
