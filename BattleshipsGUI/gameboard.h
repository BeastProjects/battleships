#ifndef GAMEBOARD_H
#define GAMEBOARD_H

#include <QtWidgets>
#include <QtGui>
#include <QGraphicsScene>

const int gridSize = 10;

class BoardCell : public QGraphicsRectItem
{
public:
    BoardCell(qreal x, qreal y, qreal width, qreal height)
        : QGraphicsRectItem(x, y, width, height)
    {
        setBrush(Qt::NoBrush);
        setPen(QPen(Qt::black, 1));
    }
};

class MissSign : public QGraphicsEllipseItem{
public:
    MissSign(qreal x, qreal y, qreal size)
        : QGraphicsEllipseItem(x, y, size, size)
    {
        setBrush(Qt::gray);
    }
};

class HitSign : public QGraphicsItem {
public:
    HitSign(int x, int y, int size)
        : QGraphicsItem(), m_x(x), m_y(y), m_size(size)
    {
    }

    QRectF boundingRect() const override {
        return QRectF(m_x, m_y, m_x + m_size, m_y + m_size); // Adjust the size of the bounding rectangle as needed
    }

    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override {
        Q_UNUSED(option);
        Q_UNUSED(widget);

        // Set the pen for drawing the lines
        QPen pen(Qt::red, 2); // Color: Black, Line Width: 2
        painter->setPen(pen);

        // Draw the "X" symbol
        painter->drawLine(m_x, m_y, m_x + m_size, m_y + m_size); // Top-left to bottom-right
        painter->drawLine(m_x, m_y + m_size, m_x + m_size, m_y); // Bottom-left to top-right
    }

private:
    int m_x;
    int m_y;
    int m_size;
};

class GameBoard
{
public:
    GameBoard(QWidget *parent = nullptr, int locX = 0, int locY = 0, int size = 400, bool myBoard = true);

    int getLocX();
    int getLocY();

    std::pair<int, int> getBoardPos(int mousePosX, int mousePosY);
    void markHit(int squarePosX, int squarePosY);
    void markMiss(int squarePosX, int squarePosY);
    void markSquare(int squarePosX, int squarePosY);
    void unmarkSquare(int squarePosX, int squarePosY);
    void setBoard(std::string boardStr);
    
private:
    QGraphicsView *view;
    QGraphicsScene *scene;

    int m_x;
    int m_y;
    int m_boardSize;
    int m_cellSize;
    bool m_isMyBoard;

};

#endif // GAMEBOARD_H





