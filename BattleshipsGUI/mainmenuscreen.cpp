#include "mainmenuscreen.h"
#include "ui_mainmenuscreen.h"

MainMenuScreen::MainMenuScreen(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MainMenuScreen)
{

    txbGameCode = new QTextEdit("", this);
    txbGameCode->setFixedSize(80, 51);
    txbGameCode->move(290, 320);

    QFont font;
    font.setFamily("Comic Sans MS");
    font.setPointSize(20);
    QString styleSheet = QString("QLineEdit { color: gold; }");
    txbGameCode->setStyleSheet(styleSheet);

    txbGameCode->setFont(font);

    ui->setupUi(this);

}

MainMenuScreen::~MainMenuScreen()
{
    delete ui;
}

void MainMenuScreen::createGame()
{
    IPayload_ptr request = std::make_shared<CreateGamePayload>("");
    IPayload_ptr response = Communicator::sendAndRecv(request);

    if (response->getCode() == VERIFICATION_CODE)
    {
        qDebug() << "Created game";
        qDebug() << response->getData();
        GameScreen* nextScreen = new GameScreen(this, true, response->getData());
        this->close();
        nextScreen->show();

    }
    else if (response->getCode() == ERROR_CODE)
    {
        qDebug() << "Failed to create new game";
    }
}

void MainMenuScreen::joinGame()
{
    std::string gameCode = this->txbGameCode->toPlainText().toStdString();
    IPayload_ptr request = std::make_shared<JoinGamePayload>(gameCode);
    IPayload_ptr response = Communicator::sendAndRecv(request);

    if (response->getCode() == VERIFICATION_CODE)
    {
        qDebug() << "Joined game";
        GameScreen* nextScreen = new GameScreen(this, false, "");
        this->close();
        nextScreen->show();
    }
    else if (response->getCode() == ERROR_CODE)
    {
        QMessageBox messageBox;
        messageBox.setText(QString::fromStdString(response->getData()));
        messageBox.setStyleSheet("QMessageBox { color: red; }");

        messageBox.exec();
        qDebug() << "Failed to join new game";
    }
}

void MainMenuScreen::signOut(std::string data)
{

    IPayload_ptr request = std::make_shared<SignoutPayload>(data);
    IPayload_ptr response = Communicator::sendAndRecv(request);

    if (response->getCode() == VERIFICATION_CODE)
    {
        this->close();
        this->parentWidget()->show();
    }
    else if (response->getCode() == ERROR_CODE)
    {
        qDebug() << "Failed to sign out new game";
    }
}





void MainMenuScreen::on_pushButton_clicked()
{
    createGame();
}


void MainMenuScreen::on_pushButton_2_clicked()
{
    joinGame();
}


void MainMenuScreen::on_pushButton_3_clicked()
{
    signOut("");
}


void MainMenuScreen::on_MainMenuScreen_finished(int result)
{
    //signOut("QUIT");
}

