#include "loginscreen.h"
#include "./ui_loginscreen.h"

LoginScreen::LoginScreen(QWidget *parent) : QMainWindow(parent), ui(new Ui::LoginScreen)
{
    ui->setupUi(this);
    QObject::connect(ui->btnLogin, &QPushButton::clicked, this, &LoginScreen::btnLogin_click);
}

LoginScreen::~LoginScreen()
{
    delete ui;
}

void LoginScreen::btnLogin_click()
{
    std::string username = ui->textEdit->toPlainText().toStdString();
    std::string password = ui->passwordTextEdit->toPlainText().toStdString();
    std::string data = username + "|" + password;
    IPayload_ptr request = std::make_shared<LoginPayload>(data);

    IPayload_ptr response = Communicator::sendAndRecv(request);

    if (response->getCode() == VERIFICATION_CODE)
    {
        MainMenuScreen* nextScreen = new MainMenuScreen(this);
        this->close();
        nextScreen->show();
    }
    else
    {
        QMessageBox messageBox;

        messageBox.setText(QString::fromStdString(response->getData()));
        messageBox.setStyleSheet("QMessageBox { color: red; }");

        messageBox.exec();
    }
}


void LoginScreen::on_btnSignup_clicked()
{
    SignupScreen* nextScreen = new SignupScreen(this);
    this->close();
    nextScreen->show();
}

