#ifndef COMMUNICATOR_H
#define COMMUNICATOR_H

#include <winsock2.h>
#include <ws2tcpip.h>
#include <string>
#include <QDebug>
#include <mutex>
#include "payloads.h"

class Communicator
{
public:
    static void connectServer(int port, std::string ip);

    static IPayload_ptr sendAndRecv(std::shared_ptr<IPayload> payload);
    static void sendMsg(IPayload_ptr request);
    static IPayload_ptr recvMsg();

private:
    static SOCKET mySocket;
    static std::mutex socketMut;

    static std::string packMsg(IPayload_ptr request);
    static IPayload_ptr analyzeMsg(std::string response);
    static void encryptOrDecrypt(std::string* msg);
};

#endif // COMMUNICATOR_H
