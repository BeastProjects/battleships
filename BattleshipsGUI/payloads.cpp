#include "payloads.h"

LoginPayload::LoginPayload(const std::string data) : IPayload(data) {}

unsigned int LoginPayload::getCode() const
{
    return LOGIN_CODE;
}

SignupPayload::SignupPayload(const std::string data) : IPayload(data) {}

unsigned int SignupPayload::getCode() const
{
    return SIGNUP_CODE;
}

SignoutPayload::SignoutPayload(const std::string data) : IPayload(data) {}

unsigned int SignoutPayload::getCode() const
{
    return SIGNOUT_CODE;
}

LeavePayload::LeavePayload(const std::string data) : IPayload(data) {}

unsigned int LeavePayload::getCode() const
{
    return LEAVE_CODE;
}

CreateGamePayload::CreateGamePayload(const std::string data) : IPayload(data) {}

unsigned int CreateGamePayload::getCode() const
{
    return CREATE_GAME_CODE;
}

GameMovePayload::GameMovePayload(const std::string data) : IPayload(data) {}

unsigned int GameMovePayload::getCode() const
{
    return GAME_MOVE_CODE;
}

JoinGamePayload::JoinGamePayload(const std::string data) : IPayload(data) {}

unsigned int JoinGamePayload::getCode() const
{
    return JOIN_GAME_CODE;
}

UpdateGamePayload::UpdateGamePayload(const std::string data) : IPayload(data) {}

unsigned int UpdateGamePayload::getCode() const
{
    return UPDATE_GAME_CODE;
}

VerificationPayload::VerificationPayload(const std::string data) : IPayload(data) {}

unsigned int VerificationPayload::getCode() const
{
    return VERIFICATION_CODE;
}

ErrorPayload::ErrorPayload(const std::string data) : IPayload(data) {}

unsigned int ErrorPayload::getCode() const
{
    return ERROR_CODE;
}

VictoryPayload::VictoryPayload(const std::string data) : IPayload(data) {}

unsigned int VictoryPayload::getCode() const
{
    return VICTORY_CODE;
}

LossPayload::LossPayload(const std::string data) : IPayload(data) {}

unsigned int LossPayload::getCode() const
{
    return LOSS_CODE;
}

StartGamePayload::StartGamePayload(const std::string data) : IPayload(data) {}

unsigned int StartGamePayload::getCode() const
{
    return START_GAME_CODE;
}
