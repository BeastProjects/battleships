#include "gameboard.h"
#include <algorithm>

const char EMPTY = '0';
const char SHIP = 'B';
const char HIT = 'X';
const char MISS = '.';

GameBoard::GameBoard(QWidget *parent, int locX, int locY, int size, bool myBoard)
{
    this->m_x = locX;
    this->m_y = locY;
    this->m_boardSize = size;
    this->m_cellSize = size / gridSize;
    this->m_isMyBoard = myBoard;
    // Create a QGraphicsView
    view = new QGraphicsView(parent);
    view->setGeometry(m_x, m_y, m_boardSize + 2, m_boardSize + 2);

    // Create the scene
    scene = new QGraphicsScene(parent);

    // Set the scene size
    scene->setSceneRect(0, 0, m_boardSize, m_boardSize); // Adjust the size as needed

    // Set the scene for the view
    view->setScene(scene);

    // Set the view background color
    view->setBackgroundBrush(Qt::lightGray); // Adjust the color as needed
}

std::pair<int, int> GameBoard::getBoardPos(int mousePosX, int mousePosY)
{
    int posX = (mousePosX - m_x) / m_cellSize;
    int posY = (mousePosY - m_y) / m_cellSize;

    if (posX <= 9 && posX >= 0 && posY <= 9 && posY >= 0)
    {
        return {posX, posY};
    }

    return {-1, -1};
}

void GameBoard::setBoard(std::string boardStr)
{
    QColor white = Qt::white;
    QColor black = Qt::black;

    for (int row = 0; row < gridSize; row++)
    {
        for (int col = 0; col < gridSize; col++)
        {
            qreal x = col * m_cellSize;
            qreal y = row * m_cellSize;

            BoardCell *cell = new BoardCell(x, y, m_cellSize, m_cellSize);

            scene->addItem(cell);

            // Sets the sign of the square(is it a hit or a miss or mine?)
            char cellSign = boardStr[row * gridSize + col];
            if(cellSign == SHIP || cellSign == HIT && m_isMyBoard)
            {
                this->markSquare(col, row);
            }
            if(cellSign == HIT)
            {
                this->markHit(col, row);
            }
            if(cellSign == MISS)
            {
                this->markMiss(col, row);
            }

        }

    }
}

// Returns the location of the board in the x axis
int GameBoard::getLocX()
{
    return this->m_x;
}

// Returns the location of the board in the y axis
int GameBoard::getLocY()
{
    return this->m_y;
}

void GameBoard::markMiss(int squarePosX, int squarePosY)
{
    if (squarePosX > gridSize - 1 || squarePosX < 0 || squarePosY > gridSize - 1 || squarePosY < 0)
    {
        return;
    }

    int squareLocX = squarePosX * m_cellSize;
    int squareLocY = squarePosY * m_cellSize;

    MissSign *miss = new MissSign(squareLocX + 3 * m_cellSize / 8, squareLocY + 3 * m_cellSize / 8, m_cellSize/4);

    this->scene->addItem(miss);
}

void GameBoard::markHit(int squarePosX, int squarePosY)
{
    if (squarePosX > gridSize - 1 || squarePosX < 0 || squarePosY > gridSize - 1 || squarePosY < 0)
    {
        return;
    }

    int squareLocX = squarePosX * m_cellSize;
    int squareLocY = squarePosY * m_cellSize;

    HitSign *square = new HitSign(squareLocX + 1, squareLocY + 1, m_cellSize - 2);

    this->scene->addItem(square);
}

void GameBoard::markSquare(int squarePosX, int squarePosY)
{
    if (squarePosX > gridSize - 1 || squarePosX < 0 || squarePosY > gridSize - 1 || squarePosY < 0)
    {
        return;
    }

    int squareLocX = squarePosX * m_cellSize;
    int squareLocY = squarePosY * m_cellSize;

    QGraphicsRectItem *square = new QGraphicsRectItem(squareLocX + 1, squareLocY + 1, m_cellSize - 2, m_cellSize - 2);
    square->setPen(QPen(Qt::green, 2));  // Set the outline color
    square->setBrush(Qt::NoBrush);

    this->scene->addItem(square);
}

void GameBoard::unmarkSquare(int squarePosX, int squarePosY)
{
    if (squarePosX > gridSize - 1 || squarePosX < 0 || squarePosY > gridSize - 1 || squarePosY < 0)
    {
        return;
    }

    int squareLocX = squarePosX * m_cellSize;
    int squareLocY = squarePosY * m_cellSize;

    QColor squareColor = Qt::white;

    QGraphicsRectItem *square = new QGraphicsRectItem(squareLocX + 1, squareLocY + 1, m_cellSize - 2, m_cellSize - 2);
    square->setPen(QPen(squareColor, 2));  // Set the outline color
    square->setBrush(Qt::NoBrush);

    this->scene->addItem(square);
}


