#include "signupscreen.h"
#include "ui_signupscreen.h"

SignupScreen::SignupScreen(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SignupScreen)
{
    ui->setupUi(this);
}

SignupScreen::~SignupScreen()
{
    delete ui;
}

void SignupScreen::on_btnReturn_clicked()
{
    close();
    this->parentWidget()->show();
}


void SignupScreen::on_btnSignup_clicked()
{
    std::string username = ui->textEdit->toPlainText().toStdString();
    std::string password = ui->passwordTextEdit->toPlainText().toStdString();
    std::string mail = ui->mailTextEdit->toPlainText().toStdString();
    std::string age = ui->ageTextEdit->toPlainText().toStdString();
    std::string mom = ui->momTextEdit->toPlainText().toStdString();
    std::string data = username + "|" + password + "|" + mail + "|" + age + "|" + mom;
    IPayload_ptr request = std::make_shared<SignupPayload>(data);

    IPayload_ptr response = Communicator::sendAndRecv(request);

    if (response->getCode() == VERIFICATION_CODE)
    {
        MainMenuScreen* nextScreen = new MainMenuScreen(this->parentWidget());
        this->close();
        nextScreen->show();
    }
    else
    {
        QMessageBox messageBox;

        messageBox.setText(QString::fromStdString(response->getData()));
        messageBox.setStyleSheet("QMessageBox { color: red; }");

        messageBox.exec();
    }
}

