#ifndef LOGINSCREEN_H
#define LOGINSCREEN_H

#include <QPushButton>
#include <QObject>
#include <QApplication>
#include <QMainWindow>
#include <QPixmap>
#include "mainmenuscreen.h"
#include "communicator.h"
#include "signupscreen.h"

QT_BEGIN_NAMESPACE
namespace Ui { class LoginScreen; }
QT_END_NAMESPACE

class LoginScreen : public QMainWindow
{
    Q_OBJECT

public:
    LoginScreen(QWidget *parent = nullptr);
    ~LoginScreen();



private slots:

    void on_btnSignup_clicked();

private:
    Ui::LoginScreen *ui;
    QPushButton* myButton;

    void btnLogin_click();

};
#endif // LOGINSCREEN_H
