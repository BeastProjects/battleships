#pragma once
#include "ipayload.h"

#define VERIFICATION_CODE 22
#define ERROR_CODE 255
#define UPDATE_GAME_CODE 44
#define JOIN_GAME_CODE 100
#define CREATE_GAME_CODE 150
#define SIGNOUT_CODE 169
#define LEAVE_CODE 170
#define LOSS_CODE 33
#define VICTORY_CODE 10
#define LOGIN_CODE 50
#define SIGNUP_CODE 60
#define GAME_MOVE_CODE 200
#define START_GAME_CODE 120

class LoginPayload  : public IPayload
{
public:
    LoginPayload(const std::string data);
    virtual unsigned int getCode() const;
};

class SignupPayload  : public IPayload
{
public:
    SignupPayload(const std::string data);
    virtual unsigned int getCode() const;
};

class SignoutPayload : public IPayload
{
public:
    SignoutPayload(const std::string data);
    virtual unsigned int getCode() const;
};

class LeavePayload : public IPayload
{
public:
    LeavePayload(const std::string data);
    virtual unsigned int getCode() const;
};

class CreateGamePayload : public IPayload
{
public:
    CreateGamePayload(const std::string data);
    virtual unsigned int getCode() const;
};

class GameMovePayload : public IPayload
{
public:
    GameMovePayload(const std::string data);
    virtual unsigned int getCode() const;
};

class JoinGamePayload : public IPayload
{
public:
    JoinGamePayload(const std::string data);
    virtual unsigned int getCode() const;
};

class UpdateGamePayload : public IPayload
{
public:
    UpdateGamePayload(const std::string data);
    virtual unsigned int getCode() const;
};

class VerificationPayload : public IPayload
{
public:
    VerificationPayload(const std::string data);
    virtual unsigned int getCode() const;
};

class ErrorPayload : public IPayload
{
public:
    ErrorPayload(const std::string data);
    virtual unsigned int getCode() const;
};

class VictoryPayload : public IPayload
{
public:
    VictoryPayload(const std::string data);
    virtual unsigned int getCode() const;
};

class LossPayload : public IPayload
{
public:
    LossPayload(const std::string data);
    virtual unsigned int getCode() const;
};

class StartGamePayload : public IPayload
{
public:
    StartGamePayload(const std::string data);
    virtual unsigned int getCode() const;
};
